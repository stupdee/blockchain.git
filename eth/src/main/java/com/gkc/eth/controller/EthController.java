package com.gkc.eth.controller;

import com.gkc.eth.config.ETHConfig;
import com.gkc.eth.util.ETHUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.web3j.crypto.Credentials;
import org.web3j.protocol.Web3j;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/eth")
public class EthController {
    private String walletFileBasePath="D:\\keystore\\";

    @Value("${trx.contract-address}")
    private String contractAddress;


    //生成账户
    @GetMapping("/createAccount")
    public String generateAddress(String password) {
        String walletFileName = null;
        try {
            walletFileName = ETHUtils.creatAccount(walletFileBasePath, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 钱包文件
        String walletFile = walletFileBasePath + walletFileName;
        System.out.println("第一步：创建的钱包KeyStore文件为: ---> " + walletFile);
        return walletFile;
    }

    //获取余额
    @GetMapping("/getBalance")
    public String getAccount(String walletFile, String password) {
        Credentials credentials = null;
        try {
            credentials = ETHUtils.getCredentials(walletFile, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 加载钱包
        Map<String, Object> map = null;
        try {
            map = ETHUtils.loadWallet(credentials);
        } catch (Exception e) {
            e.printStackTrace();
        }
        String address = String.valueOf(map.get("address"));


        Web3j web3j = null;
        try {
            web3j = ETHUtils.createETHclient(ETHConfig.API_INFURA_TOKEN);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("==================获取钱包余额");
        String blanceOf = null;
        try {
            blanceOf = ETHUtils.getBlanceOf(web3j, address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("当前账户 ETH 余额: " + blanceOf);
        return blanceOf;
    }

    //获取erc20代币余额
    @GetMapping("/getErc20Balance")
    public String getAccount(String address) {
        Web3j web3j = null;
        try {
            web3j = ETHUtils.createETHclient(ETHConfig.API_INFURA_TOKEN);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String erc20Balance = null;
        try {
            erc20Balance = ETHUtils.getERC20Balance(web3j, address, contractAddress);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("当前账户 USDT 余额: " + erc20Balance);
        return erc20Balance;
    }

    //erc10转账
    //变量可以自由处理，或者内网调用该接口
    @GetMapping("/transferErc10")
    public String transferErc10(String toAddress, String walletFile, String password, BigDecimal amount) throws Exception {
        Credentials credentials = ETHUtils.getCredentials(walletFile, password);
        Web3j web3j = ETHUtils.createETHclient(ETHConfig.API_INFURA_TOKEN);
        String txId = ETHUtils.transErc10To(web3j, credentials, amount, toAddress);
        return txId;
    }

    //erc20转账
    //变量可以自由处理，或者内网调用该接口
    @GetMapping("/transferErc20")
    public String transferErc20(String toAddress, String walletFile, String password, BigDecimal amount) throws Exception {
        Credentials credentials = ETHUtils.getCredentials(walletFile, password);
        Web3j web3j = ETHUtils.createETHclient(ETHConfig.API_INFURA_TOKEN);

        String txId = ETHUtils.transferERC20Token(web3j, toAddress, toAddress, amount, credentials, contractAddress);

        return txId;
    }

}
