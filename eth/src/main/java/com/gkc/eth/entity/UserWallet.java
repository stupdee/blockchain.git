package com.gkc.eth.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

public class UserWallet {

    /**
     * 钱包地址
     */
    private String address;

    /**
     * 钱包公钥
     */
    private BigInteger publicKe;

    /**
     * 钱包私钥
     */
    private String privateKey;

    /**
     * 以太坊币余额
     */
    private BigDecimal ether;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigInteger getPublicKe() {
        return publicKe;
    }

    public void setPublicKe(BigInteger publicKe) {
        this.publicKe = publicKe;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public BigDecimal getEther() {
        return ether;
    }

    public void setEther(BigDecimal ether) {
        this.ether = ether;
    }

    @Override
    public String toString() {
        return "UserWallet{" +
                "address='" + address + '\'' +
                ", publicKe=" + publicKe +
                ", privateKey='" + privateKey + '\'' +
                ", ether='" + ether + '\'' +
                '}';
    }
}
