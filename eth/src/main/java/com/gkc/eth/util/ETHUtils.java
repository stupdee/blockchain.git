package com.gkc.eth.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Address;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.crypto.*;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameter;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.*;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Transfer;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class ETHUtils {
    private static Logger log = LoggerFactory.getLogger(ETHUtils.class);
    private static final BigDecimal WEI = new BigDecimal(1000000);
    private static String TRANSFER = "transfer";


    /*************创建一个钱包文件**************/
    public static String creatAccount(String walletFileBasePath, String password)
            throws NoSuchAlgorithmException, NoSuchProviderException, InvalidAlgorithmParameterException, CipherException, IOException {
        String walletFileName0 = "";//文件名
        //钱包文件保持路径，请替换位自己的某文件夹路径
        walletFileName0 = WalletUtils.generateNewWalletFile(password, new File(walletFileBasePath), false);
        return walletFileName0;
    }

    public static Credentials getCredentials(String walleFilePath, String passWord) throws Exception{
        Credentials credentials = WalletUtils.loadCredentials(passWord, walleFilePath);
        return credentials;
    }

    /********加载钱包文件**********/
    public static Map<String, Object> loadWallet(Credentials credentials) throws IOException, CipherException {
        String address = credentials.getAddress();
        BigInteger publicKey = credentials.getEcKeyPair().getPublicKey();
        BigInteger privateKey = credentials.getEcKeyPair().getPrivateKey();
        System.out.println(privateKey);
        String prikey = Numeric.toHexStringWithPrefixZeroPadded(privateKey, Keys.PRIVATE_KEY_LENGTH_IN_HEX);
        System.out.println(prikey);
        System.out.println("第二步: 加载钱包信息（如下）:");
        System.out.println("-钱包地址:" + address);
        System.out.println("-私钥:" + prikey);
        System.out.println("-公钥:" + publicKey);
        Map<String, Object> map = new HashMap<>();
        map.put("address", address);
        map.put("privateKey", privateKey);
        map.put("publicKey", publicKey);
        return map;
    }

    /*******连接以太坊客户端**************/
    public static Web3j createETHclient(String token) throws IOException {
        //连接方式1：使用infura 提供的客户端
        // TODO: 2018/4/10 token更改为自己的
//        Web3j web3j = Web3j.build(new HttpService("https://mainnet.infura.io/v3/"+token));
        // 测试环境
        Web3j web3j = Web3j.build(new HttpService("https://ropsten.infura.io/v3/"+token));
        //连接方式2：使用本地客户端
//        Web3j web3j = Web3j.build(new HttpService("127.0.0.1:7545"));
        //测试是否连接成功
        String web3ClientVersion = web3j.web3ClientVersion().send().getWeb3ClientVersion();
        System.out.println("version:"+web3ClientVersion);
        return web3j;
    }

    /***********查询指定地址的余额***********/
    public static String getBlanceOf(Web3j web3j, String address) throws IOException {
        if (web3j == null){
            return "";
        }
        //第二个参数：区块的参数，建议选最新区块
        EthGetBalance balance = web3j.ethGetBalance(address, DefaultBlockParameter.valueOf("latest")).send();
        //格式转化 wei-ether
        String blanceETH = Convert.fromWei(balance.getBalance().toString(), Convert.Unit.ETHER).toPlainString().concat(" ether");
        System.out.println("余额:"+blanceETH);
        return blanceETH;
    }

    /**
     * 获取ERC-20 token指定地址余额
     *
     * @param address         查询地址
     * @param contractAddress 合约地址
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static String getERC20Balance(Web3j web3j, String address, String contractAddress) throws ExecutionException, InterruptedException {
        String methodName = "balanceOf";
        List<Type> inputParameters = new ArrayList<>();
        List<TypeReference<?>> outputParameters = new ArrayList<>();
        Address fromAddress = new Address(address);
        inputParameters.add(fromAddress);

        TypeReference<Uint256> typeReference = new TypeReference<Uint256>() {
        };
        outputParameters.add(typeReference);
        Function function = new Function(methodName, inputParameters, outputParameters);
        String data = FunctionEncoder.encode(function);
        Transaction transaction = Transaction.createEthCallTransaction(address, contractAddress, data);

        EthCall ethCall;
        BigDecimal balanceValue = BigDecimal.ZERO;
        try {
            ethCall = web3j.ethCall(transaction, DefaultBlockParameterName.LATEST).send();
            List<Type> results = FunctionReturnDecoder.decode(ethCall.getValue(), function.getOutputParameters());
            Integer value = 0;
            if(results != null && results.size()>0){
                value = Integer.parseInt(String.valueOf(results.get(0).getValue()));
            }
            balanceValue = new BigDecimal(value).divide(WEI, 6, RoundingMode.HALF_DOWN);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return balanceValue.toString();
    }


    /****************交易*****************/
    public static String transErc10To(Web3j web3j, Credentials credentials, BigDecimal amount, String addressTo) throws Exception {
        if (web3j == null) return null;
        if (credentials == null) return null ;
        //开始发送0.01 =eth到指定地址
        TransactionReceipt send = Transfer.sendFunds(web3j, credentials, addressTo, amount, Convert.Unit.SZABO).sendAsync().get();
        log.info("Transaction complete:");
        log.info("trans hash=" + send.getTransactionHash());
        log.info("from :" + send.getFrom());
        log.info("to:" + send.getTo());
        log.info("gas used=" + send.getGasUsed());
        log.info("status: " + send.getStatus());
        return send.getTransactionHash();
    }

    /**
     * erc20代币转账
     *
     * @param from            转账地址
     * @param to              收款地址
     * @param value           转账金额
     * @param contractAddress 代币合约地址
     * @return 交易哈希
     */
    public static String transferERC20Token(Web3j web3j, String from, String to,
                                            BigDecimal value, Credentials credentials, String contractAddress)
            throws ExecutionException, InterruptedException, IOException {
        //获取nonce，交易笔数
        BigInteger nonce;
        EthGetTransactionCount ethGetTransactionCount = web3j.ethGetTransactionCount(from, DefaultBlockParameterName.PENDING).send();
        if (ethGetTransactionCount == null) {
            return null;
        }
        nonce = ethGetTransactionCount.getTransactionCount();
        //gasPrice和gasLimit 都可以手动设置
        BigInteger gasPrice;
        EthGasPrice ethGasPrice = web3j.ethGasPrice().sendAsync().get();
        if (ethGasPrice == null) {
            return null;
        }
        gasPrice = ethGasPrice.getGasPrice();
        //BigInteger.valueOf(4300000L) 如果交易失败 很可能是手续费的设置问题
        BigInteger gasLimit = BigInteger.valueOf(60000L);
        //ERC20代币合约方法
//        value = value.multiply(VALUE);
        final Uint256 uint256 = new Uint256(value.multiply(BigDecimal.TEN.pow(18)).toBigInteger());
        Function function = new Function(
                TRANSFER,
                Arrays.asList(new Address(to), uint256),
                Collections.singletonList(new TypeReference<Type>() {
                }));
        //创建RawTransaction交易对象
        String encodedFunction = FunctionEncoder.encode(function);
        RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, gasPrice, gasLimit,
                contractAddress, encodedFunction);

        //签名Transaction
        byte[] signMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signMessage);
        //发送交易
        EthSendTransaction ethSendTransaction = web3j.ethSendRawTransaction(hexValue).sendAsync().get();
        String hash = ethSendTransaction.getTransactionHash();
        if (hash != null) {
            System.out.println("交易成功:  HASH: " + hash);
            return hash;
        }
        return null;
    }


}
