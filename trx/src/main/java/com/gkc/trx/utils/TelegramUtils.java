package com.gkc.trx.utils;

import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gaokecheng
 * @since 2022/5/4 下午2:37
 */
public class TelegramUtils {

    public static Boolean sendMessage(String token,String chatId,String message){
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("chat_id",chatId);
        paramMap.put("text",message);
        String url = "https://api.telegram.org/bot"+token+"/sendMessage";

        try {
            String response = HttpUtil.get(url, paramMap);
            JSONObject jsonObject = JSON.parseObject(response);
            Boolean result = (Boolean)jsonObject.get("ok");
            return result;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;


    }
}
