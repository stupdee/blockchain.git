package com.gkc.trx.utils;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.springframework.util.StringUtils;

import java.io.File;


/**
 * ZipUtils
 * 文件压缩工具类
 */

public class ZipUtils {
    public  static void toZip(String file,String targetFile,String password){

        try{
            ZipFile zipFile = null;
            if(StringUtils.isEmpty(file) || StringUtils.isEmpty(targetFile) || StringUtils.isEmpty(password)){
                return;
            }
            ZipParameters zipParameters = new ZipParameters();
            zipParameters.setEncryptFiles(true);
            zipParameters.setEncryptionMethod(EncryptionMethod.AES);
            zipParameters.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
            zipFile = new ZipFile(targetFile,password.toCharArray());
            zipFile.addFile(file,zipParameters);
            File file1 = new File(file);
            file1.delete();
        }catch (Exception e){
            System.out.println(e);
        }

    }


}