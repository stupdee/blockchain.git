package com.gkc.trx.param.trx;

import lombok.Data;

/**
 * @author gaokecheng
 * @since 2022/5/1 下午3:13
 */
@Data
public class AccountParam {
    private String address;
    private Boolean visible;
}
