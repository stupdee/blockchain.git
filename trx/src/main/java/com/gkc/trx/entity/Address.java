package com.gkc.trx.entity;

import lombok.Data;

/**
 * @author gaokecheng
 * @since 2022/4/22 下午9:21
 */
@Data
public class Address {
    private String address;
    private String privateKey;
}
